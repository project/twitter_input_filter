Twitter Input Filter
-------------------
  This module provides a new input filter [1] which allows to easily create links to site user and nodes. The syntax used to create links is similar to twitter. The string prefixed with @ symbol will create link to user. Similary the string prefixed with # will create link to node. If the node or user being referrenced is not found, a link to search page can be created optionally. This feature can be used in node body field, comments, CCK textarea field.


Author
-------
  http://drupal.org/user/328724


Sponsor
-------
  This project is being sponsored by Caucus LLC. Thanks to project lead Chaitanya Kommidi  http://caucusllc.com/


Related Modules
---------------
  [1] http://drupal.org/project/freelinking
  [2] http://drupal.org/project/glossify
  [3] http://drupal.org/project/facebook_status

Reference
----------
  [1] http://www.lullabot.com/articles/drupal_input_formats_and_filters
